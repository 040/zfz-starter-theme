<?php

/**
 * @file
 * Contains functions to alter Drupal's markup for the ZFZ Starter theme.
 */

/**
 * Implements hook_preprocess_page() for zfz_starter theme.
 *
 * Checks if the 'eu_cookie_compliance' module is enabled and sets
 * variables accordingly.
 */
function zfz_starter_preprocess_page(&$variables) {
  $moduleHandler = \Drupal::service('module_handler');

  /* Check if module eu_cookie_compliance is enabled */
  if ($moduleHandler->moduleExists('eu_cookie_compliance')) {
    $variables['eu_cookie_compliance_module'] = TRUE;

  }
  else {
    $variables['eu_cookie_compliance_module'] = TRUE;
  }
}
