import Glide from '@glidejs/glide'
;(function (Drupal, once) {
	Drupal.behaviors.glideBehavior = {
		attach: function (context) {
			once('glide', '.glide', context).forEach(function (glide) {
				new Glide(glide, {
					type: 'slider',
					autoplay: 5000,
					controls: true,
					perView: 1,
				}).mount()
			})
		},
	}
})(Drupal, once)