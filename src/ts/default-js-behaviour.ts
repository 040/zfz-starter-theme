declare var Drupal: any;
declare var once: Function;

/**
 * Attaches the default JavaScript behavior to elements within the specified context.
 *
 * This behavior logs a message to the console when executed.
 *
 * @param {object} context - The context to which this behavior is applied.
 */
Drupal.behaviors.defaultJSBehaviour = {
  attach: function (context: object) {
    /**
     * Executes a callback function once for each element in the specified context.
     *
     * This function ensures that the callback is invoked only once for each element,
     * preventing duplicate executions.
     *
     * @param {string} key - A unique identifier for this specific callback execution.
     * @param {string} selector - A CSS selector string specifying the elements to target.
     * @param {Function} callback - The callback function to execute for each matching element.
     * @param {object} [context=document] - The context within which to search for elements.
     */
    once("defaultJSBehaviour", "body").forEach(function (
      element: HTMLBodyElement
    ) {
      console.log("default JS behaviour loaded.");
    });
  },
};
