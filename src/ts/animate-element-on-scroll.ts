declare var Drupal: any;
declare var once: Function;

/**
 * Attaches the default JavaScript behavior to elements within the specified context.
 *
 * This behavior logs a message to the console when executed.
 *
 * @param {object} context - The context to which this behavior is applied.
 */
Drupal.behaviors.showElementOnScroll = {
  attach: function (context: object) {
    /**
     * Executes a callback function once for each element in the specified context.
     *
     * This function ensures that the callback is invoked only once for each element,
     * preventing duplicate executions.
     *
     * @param {string} key - A unique identifier for this specific callback execution.
     * @param {string} selector - A CSS selector string specifying the elements to target.
     * @param {Function} callback - The callback function to execute for each matching element.
     * @param {object} [context=document] - The context within which to search for elements.
     */
    once("showElementOnScroll", "body").forEach(function (
      element: HTMLBodyElement
    ) {
      // Get all the elements you want to show on scroll
      const targets: NodeListOf<Element> = document.querySelectorAll(
        ".js-animate-on-scroll"
      );

      // Callback for IntersectionObserver
      const callback: IntersectionObserverCallback = function (
        entries: IntersectionObserverEntry[]
      ) {
        entries.forEach((entry: IntersectionObserverEntry) => {
          // @ts-ignore
          const animationType = entry.target.dataset.animateType;

          // Is the element in the viewport?
          if (entry.isIntersecting) {
            // Add the fadeIn class:
            entry.target.classList.add(animationType);
          } else {
            // Otherwise remove the fadein class
            entry.target.classList.remove(animationType);
          }
        });
      };

      // Set up a new observer
      const observer: IntersectionObserver = new IntersectionObserver(callback);

      // Loop through each of the target
      targets.forEach(function (target: Element) {
        // Add the element to the watcher
        observer.observe(target);
      });
    });
  },
};
