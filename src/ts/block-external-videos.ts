declare var Drupal: any;
declare var once: Function;

(function (Drupal: any, once: Function) {
    let videoEmbeds = document.querySelectorAll('.external-video-js');

    Drupal.behaviors.blockExternalVideos = {
        attach: function (context: object) {
            if (!Drupal.eu_cookie_compliance.getAcceptedCategories().includes("marketing")) {
                if (videoEmbeds.length) {
                    videoEmbeds.forEach((videoEmbed: Element) => {
                        (videoEmbed as HTMLElement).classList.add("third-party-video-blocked");
                    });
                }
            } else {
                if (videoEmbeds.length) {
                    videoEmbeds.forEach((videoEmbed: Element) => {
                        let videoEmbedTemplate = (videoEmbed as HTMLElement).querySelector(".embed-video-template") as HTMLTemplateElement;
                        if (videoEmbedTemplate) {
                            let clone = videoEmbedTemplate.content.cloneNode(true);
                            (videoEmbed as HTMLElement).appendChild(clone);
                        }
                    });
                }
            }
        }
    };
})(Drupal, once);