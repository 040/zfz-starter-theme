/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/ts/block-external-videos.ts":
/*!*****************************************!*\
  !*** ./src/ts/block-external-videos.ts ***!
  \*****************************************/
/***/ (() => {

eval("\n\n(function (Drupal, once) {\n  var videoEmbeds = document.querySelectorAll('.external-video-js');\n  Drupal.behaviors.blockExternalVideos = {\n    attach: function attach(context) {\n      if (!Drupal.eu_cookie_compliance.getAcceptedCategories().includes(\"marketing\")) {\n        if (videoEmbeds.length) {\n          videoEmbeds.forEach(function (videoEmbed) {\n            videoEmbed.classList.add(\"third-party-video-blocked\");\n          });\n        }\n      } else {\n        if (videoEmbeds.length) {\n          videoEmbeds.forEach(function (videoEmbed) {\n            var videoEmbedTemplate = videoEmbed.querySelector(\".embed-video-template\");\n            if (videoEmbedTemplate) {\n              var clone = videoEmbedTemplate.content.cloneNode(true);\n              videoEmbed.appendChild(clone);\n            }\n          });\n        }\n      }\n    }\n  };\n})(Drupal, once);//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvdHMvYmxvY2stZXh0ZXJuYWwtdmlkZW9zLnRzIiwibWFwcGluZ3MiOiJBQUFhOztBQUNiLENBQUMsVUFBVUEsTUFBTSxFQUFFQyxJQUFJLEVBQUU7RUFDckIsSUFBSUMsV0FBVyxHQUFHQyxRQUFRLENBQUNDLGdCQUFnQixDQUFDLG9CQUFvQixDQUFDO0VBQ2pFSixNQUFNLENBQUNLLFNBQVMsQ0FBQ0MsbUJBQW1CLEdBQUc7SUFDbkNDLE1BQU0sRUFBRSxTQUFBQSxPQUFVQyxPQUFPLEVBQUU7TUFDdkIsSUFBSSxDQUFDUixNQUFNLENBQUNTLG9CQUFvQixDQUFDQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUNDLFFBQVEsQ0FBQyxXQUFXLENBQUMsRUFBRTtRQUM1RSxJQUFJVCxXQUFXLENBQUNVLE1BQU0sRUFBRTtVQUNwQlYsV0FBVyxDQUFDVyxPQUFPLENBQUMsVUFBQ0MsVUFBVSxFQUFLO1lBQ2hDQSxVQUFVLENBQUNDLFNBQVMsQ0FBQ0MsR0FBRyxDQUFDLDJCQUEyQixDQUFDO1VBQ3pELENBQUMsQ0FBQztRQUNOO01BQ0osQ0FBQyxNQUNJO1FBQ0QsSUFBSWQsV0FBVyxDQUFDVSxNQUFNLEVBQUU7VUFDcEJWLFdBQVcsQ0FBQ1csT0FBTyxDQUFDLFVBQUNDLFVBQVUsRUFBSztZQUNoQyxJQUFJRyxrQkFBa0IsR0FBR0gsVUFBVSxDQUFDSSxhQUFhLENBQUMsdUJBQXVCLENBQUM7WUFDMUUsSUFBSUQsa0JBQWtCLEVBQUU7Y0FDcEIsSUFBSUUsS0FBSyxHQUFHRixrQkFBa0IsQ0FBQ0csT0FBTyxDQUFDQyxTQUFTLENBQUMsSUFBSSxDQUFDO2NBQ3REUCxVQUFVLENBQUNRLFdBQVcsQ0FBQ0gsS0FBSyxDQUFDO1lBQ2pDO1VBQ0osQ0FBQyxDQUFDO1FBQ047TUFDSjtJQUNKO0VBQ0osQ0FBQztBQUNMLENBQUMsRUFBRW5CLE1BQU0sRUFBRUMsSUFBSSxDQUFDIiwic291cmNlcyI6WyJ3ZWJwYWNrOi8vLy4vc3JjL3RzL2Jsb2NrLWV4dGVybmFsLXZpZGVvcy50cz83ZTNlIl0sInNvdXJjZXNDb250ZW50IjpbIlwidXNlIHN0cmljdFwiO1xuKGZ1bmN0aW9uIChEcnVwYWwsIG9uY2UpIHtcbiAgICBsZXQgdmlkZW9FbWJlZHMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCcuZXh0ZXJuYWwtdmlkZW8tanMnKTtcbiAgICBEcnVwYWwuYmVoYXZpb3JzLmJsb2NrRXh0ZXJuYWxWaWRlb3MgPSB7XG4gICAgICAgIGF0dGFjaDogZnVuY3Rpb24gKGNvbnRleHQpIHtcbiAgICAgICAgICAgIGlmICghRHJ1cGFsLmV1X2Nvb2tpZV9jb21wbGlhbmNlLmdldEFjY2VwdGVkQ2F0ZWdvcmllcygpLmluY2x1ZGVzKFwibWFya2V0aW5nXCIpKSB7XG4gICAgICAgICAgICAgICAgaWYgKHZpZGVvRW1iZWRzLmxlbmd0aCkge1xuICAgICAgICAgICAgICAgICAgICB2aWRlb0VtYmVkcy5mb3JFYWNoKCh2aWRlb0VtYmVkKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB2aWRlb0VtYmVkLmNsYXNzTGlzdC5hZGQoXCJ0aGlyZC1wYXJ0eS12aWRlby1ibG9ja2VkXCIpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBpZiAodmlkZW9FbWJlZHMubGVuZ3RoKSB7XG4gICAgICAgICAgICAgICAgICAgIHZpZGVvRW1iZWRzLmZvckVhY2goKHZpZGVvRW1iZWQpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGxldCB2aWRlb0VtYmVkVGVtcGxhdGUgPSB2aWRlb0VtYmVkLnF1ZXJ5U2VsZWN0b3IoXCIuZW1iZWQtdmlkZW8tdGVtcGxhdGVcIik7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodmlkZW9FbWJlZFRlbXBsYXRlKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGNsb25lID0gdmlkZW9FbWJlZFRlbXBsYXRlLmNvbnRlbnQuY2xvbmVOb2RlKHRydWUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZpZGVvRW1iZWQuYXBwZW5kQ2hpbGQoY2xvbmUpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9O1xufSkoRHJ1cGFsLCBvbmNlKTtcbiJdLCJuYW1lcyI6WyJEcnVwYWwiLCJvbmNlIiwidmlkZW9FbWJlZHMiLCJkb2N1bWVudCIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJiZWhhdmlvcnMiLCJibG9ja0V4dGVybmFsVmlkZW9zIiwiYXR0YWNoIiwiY29udGV4dCIsImV1X2Nvb2tpZV9jb21wbGlhbmNlIiwiZ2V0QWNjZXB0ZWRDYXRlZ29yaWVzIiwiaW5jbHVkZXMiLCJsZW5ndGgiLCJmb3JFYWNoIiwidmlkZW9FbWJlZCIsImNsYXNzTGlzdCIsImFkZCIsInZpZGVvRW1iZWRUZW1wbGF0ZSIsInF1ZXJ5U2VsZWN0b3IiLCJjbG9uZSIsImNvbnRlbnQiLCJjbG9uZU5vZGUiLCJhcHBlbmRDaGlsZCJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/ts/block-external-videos.ts\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module can't be inlined because the eval-source-map devtool is used.
/******/ 	var __webpack_exports__ = {};
/******/ 	__webpack_modules__["./src/ts/block-external-videos.ts"]();
/******/ 	
/******/ })()
;