/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/ts/default-js-behaviour.ts":
/*!****************************************!*\
  !*** ./src/ts/default-js-behaviour.ts ***!
  \****************************************/
/***/ (() => {

eval("\n\n/**\n * Attaches the default JavaScript behavior to elements within the specified context.\n *\n * This behavior logs a message to the console when executed.\n *\n * @param {object} context - The context to which this behavior is applied.\n */\nDrupal.behaviors.defaultJSBehaviour = {\n  attach: function attach(context) {\n    /**\n     * Executes a callback function once for each element in the specified context.\n     *\n     * This function ensures that the callback is invoked only once for each element,\n     * preventing duplicate executions.\n     *\n     * @param {string} key - A unique identifier for this specific callback execution.\n     * @param {string} selector - A CSS selector string specifying the elements to target.\n     * @param {Function} callback - The callback function to execute for each matching element.\n     * @param {object} [context=document] - The context within which to search for elements.\n     */\n    once(\"defaultJSBehaviour\", \"body\").forEach(function (element) {\n      console.log(\"default JS behaviour loaded.\");\n    });\n  }\n};//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvdHMvZGVmYXVsdC1qcy1iZWhhdmlvdXIudHMiLCJtYXBwaW5ncyI6IkFBQWE7O0FBQ2I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQUEsTUFBTSxDQUFDQyxTQUFTLENBQUNDLGtCQUFrQixHQUFHO0VBQ2xDQyxNQUFNLEVBQUUsU0FBQUEsT0FBVUMsT0FBTyxFQUFFO0lBQ3ZCO0FBQ1I7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7SUFDUUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLE1BQU0sQ0FBQyxDQUFDQyxPQUFPLENBQUMsVUFBVUMsT0FBTyxFQUFFO01BQzFEQyxPQUFPLENBQUNDLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQztJQUMvQyxDQUFDLENBQUM7RUFDTjtBQUNKLENBQUMiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvdHMvZGVmYXVsdC1qcy1iZWhhdmlvdXIudHM/NzI3YiJdLCJzb3VyY2VzQ29udGVudCI6WyJcInVzZSBzdHJpY3RcIjtcbi8qKlxuICogQXR0YWNoZXMgdGhlIGRlZmF1bHQgSmF2YVNjcmlwdCBiZWhhdmlvciB0byBlbGVtZW50cyB3aXRoaW4gdGhlIHNwZWNpZmllZCBjb250ZXh0LlxuICpcbiAqIFRoaXMgYmVoYXZpb3IgbG9ncyBhIG1lc3NhZ2UgdG8gdGhlIGNvbnNvbGUgd2hlbiBleGVjdXRlZC5cbiAqXG4gKiBAcGFyYW0ge29iamVjdH0gY29udGV4dCAtIFRoZSBjb250ZXh0IHRvIHdoaWNoIHRoaXMgYmVoYXZpb3IgaXMgYXBwbGllZC5cbiAqL1xuRHJ1cGFsLmJlaGF2aW9ycy5kZWZhdWx0SlNCZWhhdmlvdXIgPSB7XG4gICAgYXR0YWNoOiBmdW5jdGlvbiAoY29udGV4dCkge1xuICAgICAgICAvKipcbiAgICAgICAgICogRXhlY3V0ZXMgYSBjYWxsYmFjayBmdW5jdGlvbiBvbmNlIGZvciBlYWNoIGVsZW1lbnQgaW4gdGhlIHNwZWNpZmllZCBjb250ZXh0LlxuICAgICAgICAgKlxuICAgICAgICAgKiBUaGlzIGZ1bmN0aW9uIGVuc3VyZXMgdGhhdCB0aGUgY2FsbGJhY2sgaXMgaW52b2tlZCBvbmx5IG9uY2UgZm9yIGVhY2ggZWxlbWVudCxcbiAgICAgICAgICogcHJldmVudGluZyBkdXBsaWNhdGUgZXhlY3V0aW9ucy5cbiAgICAgICAgICpcbiAgICAgICAgICogQHBhcmFtIHtzdHJpbmd9IGtleSAtIEEgdW5pcXVlIGlkZW50aWZpZXIgZm9yIHRoaXMgc3BlY2lmaWMgY2FsbGJhY2sgZXhlY3V0aW9uLlxuICAgICAgICAgKiBAcGFyYW0ge3N0cmluZ30gc2VsZWN0b3IgLSBBIENTUyBzZWxlY3RvciBzdHJpbmcgc3BlY2lmeWluZyB0aGUgZWxlbWVudHMgdG8gdGFyZ2V0LlxuICAgICAgICAgKiBAcGFyYW0ge0Z1bmN0aW9ufSBjYWxsYmFjayAtIFRoZSBjYWxsYmFjayBmdW5jdGlvbiB0byBleGVjdXRlIGZvciBlYWNoIG1hdGNoaW5nIGVsZW1lbnQuXG4gICAgICAgICAqIEBwYXJhbSB7b2JqZWN0fSBbY29udGV4dD1kb2N1bWVudF0gLSBUaGUgY29udGV4dCB3aXRoaW4gd2hpY2ggdG8gc2VhcmNoIGZvciBlbGVtZW50cy5cbiAgICAgICAgICovXG4gICAgICAgIG9uY2UoXCJkZWZhdWx0SlNCZWhhdmlvdXJcIiwgXCJib2R5XCIpLmZvckVhY2goZnVuY3Rpb24gKGVsZW1lbnQpIHtcbiAgICAgICAgICAgIGNvbnNvbGUubG9nKFwiZGVmYXVsdCBKUyBiZWhhdmlvdXIgbG9hZGVkLlwiKTtcbiAgICAgICAgfSk7XG4gICAgfSxcbn07XG4iXSwibmFtZXMiOlsiRHJ1cGFsIiwiYmVoYXZpb3JzIiwiZGVmYXVsdEpTQmVoYXZpb3VyIiwiYXR0YWNoIiwiY29udGV4dCIsIm9uY2UiLCJmb3JFYWNoIiwiZWxlbWVudCIsImNvbnNvbGUiLCJsb2ciXSwic291cmNlUm9vdCI6IiJ9\n//# sourceURL=webpack-internal:///./src/ts/default-js-behaviour.ts\n");

/***/ }),

/***/ "./src/css/fonts.css":
/*!***************************!*\
  !*** ./src/css/fonts.css ***!
  \***************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY3NzL2ZvbnRzLmNzcyIsIm1hcHBpbmdzIjoiO0FBQUEiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY3NzL2ZvbnRzLmNzcz84MjNhIl0sInNvdXJjZXNDb250ZW50IjpbIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpblxuZXhwb3J0IHt9OyJdLCJuYW1lcyI6W10sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/css/fonts.css\n");

/***/ }),

/***/ "./src/css/main.css":
/*!**************************!*\
  !*** ./src/css/main.css ***!
  \**************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY3NzL21haW4uY3NzIiwibWFwcGluZ3MiOiI7QUFBQSIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3NyYy9jc3MvbWFpbi5jc3M/MzdiMCJdLCJzb3VyY2VzQ29udGVudCI6WyIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW5cbmV4cG9ydCB7fTsiXSwibmFtZXMiOltdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./src/css/main.css\n");

/***/ }),

/***/ "./src/css/ckeditor.css":
/*!******************************!*\
  !*** ./src/css/ckeditor.css ***!
  \******************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n// extracted by mini-css-extract-plugin\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9zcmMvY3NzL2NrZWRpdG9yLmNzcyIsIm1hcHBpbmdzIjoiO0FBQUEiLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9zcmMvY3NzL2NrZWRpdG9yLmNzcz9hMjk5Il0sInNvdXJjZXNDb250ZW50IjpbIi8vIGV4dHJhY3RlZCBieSBtaW5pLWNzcy1leHRyYWN0LXBsdWdpblxuZXhwb3J0IHt9OyJdLCJuYW1lcyI6W10sInNvdXJjZVJvb3QiOiIifQ==\n//# sourceURL=webpack-internal:///./src/css/ckeditor.css\n");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = __webpack_modules__;
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/chunk loaded */
/******/ 	(() => {
/******/ 		var deferred = [];
/******/ 		__webpack_require__.O = (result, chunkIds, fn, priority) => {
/******/ 			if(chunkIds) {
/******/ 				priority = priority || 0;
/******/ 				for(var i = deferred.length; i > 0 && deferred[i - 1][2] > priority; i--) deferred[i] = deferred[i - 1];
/******/ 				deferred[i] = [chunkIds, fn, priority];
/******/ 				return;
/******/ 			}
/******/ 			var notFulfilled = Infinity;
/******/ 			for (var i = 0; i < deferred.length; i++) {
/******/ 				var [chunkIds, fn, priority] = deferred[i];
/******/ 				var fulfilled = true;
/******/ 				for (var j = 0; j < chunkIds.length; j++) {
/******/ 					if ((priority & 1 === 0 || notFulfilled >= priority) && Object.keys(__webpack_require__.O).every((key) => (__webpack_require__.O[key](chunkIds[j])))) {
/******/ 						chunkIds.splice(j--, 1);
/******/ 					} else {
/******/ 						fulfilled = false;
/******/ 						if(priority < notFulfilled) notFulfilled = priority;
/******/ 					}
/******/ 				}
/******/ 				if(fulfilled) {
/******/ 					deferred.splice(i--, 1)
/******/ 					var r = fn();
/******/ 					if (r !== undefined) result = r;
/******/ 				}
/******/ 			}
/******/ 			return result;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/jsonp chunk loading */
/******/ 	(() => {
/******/ 		// no baseURI
/******/ 		
/******/ 		// object to store loaded and loading chunks
/******/ 		// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 		// [resolve, reject, Promise] = chunk loading, 0 = chunk loaded
/******/ 		var installedChunks = {
/******/ 			"/js/default-js-behaviour": 0,
/******/ 			"ckeditor": 0,
/******/ 			"css/main": 0,
/******/ 			"css/fonts": 0
/******/ 		};
/******/ 		
/******/ 		// no chunk on demand loading
/******/ 		
/******/ 		// no prefetching
/******/ 		
/******/ 		// no preloaded
/******/ 		
/******/ 		// no HMR
/******/ 		
/******/ 		// no HMR manifest
/******/ 		
/******/ 		__webpack_require__.O.j = (chunkId) => (installedChunks[chunkId] === 0);
/******/ 		
/******/ 		// install a JSONP callback for chunk loading
/******/ 		var webpackJsonpCallback = (parentChunkLoadingFunction, data) => {
/******/ 			var [chunkIds, moreModules, runtime] = data;
/******/ 			// add "moreModules" to the modules object,
/******/ 			// then flag all "chunkIds" as loaded and fire callback
/******/ 			var moduleId, chunkId, i = 0;
/******/ 			if(chunkIds.some((id) => (installedChunks[id] !== 0))) {
/******/ 				for(moduleId in moreModules) {
/******/ 					if(__webpack_require__.o(moreModules, moduleId)) {
/******/ 						__webpack_require__.m[moduleId] = moreModules[moduleId];
/******/ 					}
/******/ 				}
/******/ 				if(runtime) var result = runtime(__webpack_require__);
/******/ 			}
/******/ 			if(parentChunkLoadingFunction) parentChunkLoadingFunction(data);
/******/ 			for(;i < chunkIds.length; i++) {
/******/ 				chunkId = chunkIds[i];
/******/ 				if(__webpack_require__.o(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 					installedChunks[chunkId][0]();
/******/ 				}
/******/ 				installedChunks[chunkId] = 0;
/******/ 			}
/******/ 			return __webpack_require__.O(result);
/******/ 		}
/******/ 		
/******/ 		var chunkLoadingGlobal = self["webpackChunk"] = self["webpackChunk"] || [];
/******/ 		chunkLoadingGlobal.forEach(webpackJsonpCallback.bind(null, 0));
/******/ 		chunkLoadingGlobal.push = webpackJsonpCallback.bind(null, chunkLoadingGlobal.push.bind(chunkLoadingGlobal));
/******/ 	})();
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module depends on other loaded chunks and execution need to be delayed
/******/ 	__webpack_require__.O(undefined, ["ckeditor","css/main","css/fonts"], () => (__webpack_require__("./src/ts/default-js-behaviour.ts")))
/******/ 	__webpack_require__.O(undefined, ["ckeditor","css/main","css/fonts"], () => (__webpack_require__("./src/css/fonts.css")))
/******/ 	__webpack_require__.O(undefined, ["ckeditor","css/main","css/fonts"], () => (__webpack_require__("./src/css/main.css")))
/******/ 	var __webpack_exports__ = __webpack_require__.O(undefined, ["ckeditor","css/main","css/fonts"], () => (__webpack_require__("./src/css/ckeditor.css")))
/******/ 	__webpack_exports__ = __webpack_require__.O(__webpack_exports__);
/******/ 	
/******/ })()
;