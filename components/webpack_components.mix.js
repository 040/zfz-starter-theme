const componentDir = "./components/";
let mix = require("laravel-mix");
const components = [
  { name: 'button', path: 'forms/button' },
];

components.forEach((component) => {
  mix.ts(`${componentDir}/${component.path}/${component.name}.ts`, `${componentDir}/${component.path}/`)
});
