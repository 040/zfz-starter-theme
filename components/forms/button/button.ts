// @ts-ignore
Drupal.behaviors.button = {
  /**
   * Attach behavior to elements.
   *
   * @param {Document} context - The context document object.
   */
  attach(context: Document) {
    const buttons: NodeListOf<HTMLButtonElement> = context.querySelectorAll(
      '[data-component-id="zfz_starter:button"]'
    );

    if (!buttons.length) {
      return;
    }

    buttons.forEach((button: HTMLButtonElement) => {
      button.addEventListener("click", (event: Event) => {
        event.preventDefault();
        console.log("Button clicked... ", button);
      });
    });
  },
};
