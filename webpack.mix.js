const themeDir = "./";
let mix = require("laravel-mix");
require("mix-tailwindcss");

mix
  .setPublicPath("./dist/")
  .setResourceRoot("/app/themes/zfz_starter/dist/")
  .webpackConfig({
    output: {
      publicPath: "/app/themes/zfz_starter/dist/",
    },
  })
  // CSS
  .css(`${themeDir}/src/css/fonts.css`, `${themeDir}/dist/css`)
  .minify(`${themeDir}/dist/css/fonts.css`)
  .css(`${themeDir}/src/css/main.css`, `${themeDir}/dist/css`)
  .tailwind()
  .minify(`${themeDir}/dist/css/main.css`)
  .css(`${themeDir}/src/css/ckeditor.css`, `${themeDir}/dist`)
  .tailwind()
  .minify(`${themeDir}/dist/ckeditor.css`)

  // TypeScript
  .ts(`${themeDir}/src/ts/default-js-behaviour.ts`, `${themeDir}/dist/js`)
  .minify(`${themeDir}/dist/js/default-js-behaviour.js`)

  .ts(`${themeDir}/src/ts/animate-element-on-scroll.ts`, `${themeDir}/dist/js`)
  .minify(`${themeDir}/dist/js/animate-element-on-scroll.js`)

  .ts(`${themeDir}/src/ts/block-external-videos.ts`, `${themeDir}/dist/js`)
  .minify(`${themeDir}/dist/js/block-external-videos.js`)

  // JavaScript
  .minify(`${themeDir}/dist/js/glide.js`)
  .options({
    processCssUrls: false,
  })
  .sourceMaps();
