/* Typography */
const typography = require("./typography.config.js");

module.exports = {
  theme: {
    extend: {
      typography: (theme) => ({
        /* For all screens */
        DEFAULT: {
          css: {},
        },
        // Typography styling for screens below 640px (1em = 16px)
        base: {
          css: {},
        },
        // Typography styling for 640px screens and above (1em = 18px)
        lg: {
          css: {},
        },
        white: {
          css: {
            "*": {
              color: "white",
            }
          },
        },
      }),
    },
  },
  plugins: [require("@tailwindcss/typography")],
};
